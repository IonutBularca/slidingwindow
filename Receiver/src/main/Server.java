package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Server extends Thread {
	private Socket socket;
	private static Socket socketForSend;

	public Server(Socket socket, Socket sForSend) {
		this.socket = socket;
		this.socketForSend = sForSend;
	}

	@Override
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socketForSend.getOutputStream(), true);
			while (true) {
				String echoString = input.readLine();
				if (echoString != null) {
					System.out.println("Recived from client:" + echoString);

					output.println("ACK " + echoString);
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
