package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import utils.Utils;

public class Main {

	public static void main(String[] args) throws InterruptedException {

		int vectDim = 0;
		int windowDim = 0;
		List<Integer> packs = new ArrayList<Integer>();
		List<Integer> send = new ArrayList<Integer>();
		List<Boolean> staredThread = new ArrayList<Boolean>();

		System.out.println("Dimensiunea vectorului: ");

		Scanner sc = new Scanner(System.in);
		vectDim = sc.nextInt();

		System.out.println("Dimensiunea ferestrei: ");
		windowDim = sc.nextInt();

		for (int i = 0; i < vectDim; i++) {
			packs.add(i);
			send.add(0);
			staredThread.add(false);
		}

		String fail;

		System.out.println("Pachetele ce vor esua(separate prin spatiu): ");
		sc.nextLine();
		fail = sc.nextLine();

		String[] fails = fail.split(" ");
		for (String s : fails) {
			send.set(Integer.parseInt(s), -1);
		}

		Integer left = 0;
		Integer right = left + windowDim;
		// Preluarea mesajelor de la receiver
		Utils.receiveAck(send);

		while (true) {

			for (int i = left; i <= right; i++) {
				if (!staredThread.get(i)) {
					Utils.send(i, packs.get(i), send);
					staredThread.set(i, true);
				}
			}
			
			
			while (left < send.size()-1 && (send.get(left) == 1)) {
				left++;
				if (right < send.size() - 1) {
					right++;
				}

			}

			
			if (left >= send.size())
				break;
		}
		System.out.println("FIN");
		sc.close();
	}

}
