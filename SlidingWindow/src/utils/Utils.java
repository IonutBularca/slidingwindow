package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Utils {

	public static void receiveAck(List<Integer> list) {

		Thread newTh = new Thread(new Runnable() {

			@Override
			public void run() {
				Socket socket = null;
				try {
					socket = new Socket("127.0.0.1", 5001);

				} catch (IOException e1) {
					e1.printStackTrace();
				}
				try {
					ReentrantLock lock = new ReentrantLock();
					BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					while (true) {
						String echoString = input.readLine();
						if (echoString != null) {
							System.out.println("Recived from client:" + echoString);
							String[] tokens = echoString.split(" ");
							lock.lock();
							list.set(Integer.parseInt(tokens[1]), list.get(Integer.parseInt(tokens[1])) + 1);
							lock.unlock();
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}
		});
		newTh.start();

	}

	public static void send(Integer index, Integer mess, List<Integer> send) {

		Thread newTh = new Thread(new Runnable() {

			@Override
			public void run() {
				Socket socket = null;
				try {
					socket = new Socket("127.0.0.1", 5000);

				} catch (IOException e1) {
					e1.printStackTrace();
				}
				try {
					PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
					while (true) {
						System.out.println("Sending " + index);
						if(send.get(index)>=0) {
							output.println(index.intValue());
						}else {
							   send.set(index,0);
							}
						
						Thread.sleep(150);
						if (send.get(index) == 1) {
							return;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}
		});
		newTh.start();

	}



}
